/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ecole.model;

/**
 *
 * @author Administrateur
 */
public class Cours {
    
  private String date;
  private String dureeEnMinute;
  private String description;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDureeEnMinute() {
        return dureeEnMinute;
    }

    public void setDureeEnMinute(String dureeEnMinute) {
        this.dureeEnMinute = dureeEnMinute;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
     public Cours(){
   } 
}
