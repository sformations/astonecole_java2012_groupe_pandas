/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ecole.model;

import java.awt.Image;

/**
 *
 * @author Administrateur
 */
public class Etudiant extends Personne {
    
    private String dateNaissance;

    public String getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(String dateNaissance) {
        this.dateNaissance = dateNaissance;
    }
   
     public Etudiant(){                 
   } 

    public Etudiant(String dateNaissance, String nom, String prenom, String email, Image photo, Adresse adresse, LoginInfos loginInfo) {
        super(nom, prenom, email, photo, adresse, loginInfo);
        this.dateNaissance = dateNaissance;
    }
     
}
