/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ecole.model;

/**
 *
 * @author Administrateur
 */
public class LoginInfos {
    
  private String motDePasse;
  private String login;

    public String getMotDePasse() {
        return motDePasse;
    }

    public void setMotDePasse(String motDePasse) {        
        this.motDePasse = motDePasse;
    }

    public LoginInfos(String motDePasse, String login) {
        this.motDePasse = motDePasse;
        this.login = login;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }
    
    public LoginInfos(){
   } 
}
