/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ecole.backingbean;

import com.ecole.dao.LoginInfoDao;
import com.ecole.model.LoginInfos;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Administrateur
 */
public class LoginInfosBackingBean {

    /**
     * Creates a new instance of LoginInfosBackingBean
     */
    public LoginInfosBackingBean() {
    }
    public static LoginInfos loginInfosBB() throws SQLException {
        LoginInfoDao lDao=new LoginInfoDao();       
        
        ResultSet result = lDao.findLogin("lpierre");
        
        result.next();
        LoginInfos dbloginInfos=new LoginInfos();
        
        dbloginInfos.setLogin(result.getString("login"));
        dbloginInfos.setMotDePasse(result.getString("password"));
        return dbloginInfos;
        
    }
//    public static void main(String arg[]) throws SQLException{
////        String login="";
//        LoginInfoDao lDao=new LoginInfoDao();
//        ResultSet result;
//        result = lDao.findLogin("lpierre");
//        LoginInfos dblg = new LoginInfos();
//        dblg=loginInfoBB();        
//        System.out.println("login :"+dblg.getLogin());
//        System.out.println("pwd :"+dblg.getMotDePasse());
//    }
}
