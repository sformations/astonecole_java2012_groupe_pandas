/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ecole.model;

import java.awt.Image;

/**
 *
 * @author Administrateur
 */
public class Personne {
    
  private String nom;
  private String prenom;
  private String email;
  private Image photo;
  private Adresse adresse;
  private LoginInfos loginInfo;

  public Personne(String nom, String prenom, String email, Image photo, Adresse adresse, LoginInfos loginInfo) {
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
        this.photo = photo;
        this.adresse = adresse;
        this.loginInfo = loginInfo;
    }
  		
   public Personne(){
   } 

    public LoginInfos getLoginInfo() {
        return loginInfo;
    }

    public void setLoginInfo(LoginInfos loginInfo) {
        this.loginInfo = loginInfo;
    }

    public Adresse getAdresse() {
        return adresse;
    }

    public void setAdresse(Adresse adresse) {
        this.adresse = adresse;
    }
  
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getEmail() {
        return email;
    }

    public Image getPhoto() {
        return photo;
    }

    public void setPhoto(Image photo) {
        this.photo = photo;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
