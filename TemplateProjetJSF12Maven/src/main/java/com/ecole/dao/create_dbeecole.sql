SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `eecole` DEFAULT CHARACTER SET utf8 ;
SHOW WARNINGS;
USE `eecole` ;

-- -----------------------------------------------------
-- Table `eecole`.`personne`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `eecole`.`personne` ;

SHOW WARNINGS;
CREATE  TABLE IF NOT EXISTS `eecole`.`personne` (
  `id` INT(11) NOT NULL ,
  `nom` VARCHAR(45) NOT NULL ,
  `prenom` VARCHAR(45) NOT NULL ,
  `email` VARCHAR(255) NULL DEFAULT NULL ,
  `photo` BLOB NULL DEFAULT NULL ,
  `type` VARCHAR(3) NOT NULL ,
  `datenaissance` DATETIME NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `eecole`.`adresse`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `eecole`.`adresse` ;

SHOW WARNINGS;
CREATE  TABLE IF NOT EXISTS `eecole`.`adresse` (
  `rue` VARCHAR(20) NOT NULL ,
  `codepostal` VARCHAR(5) NOT NULL ,
  `ville` VARCHAR(45) NOT NULL ,
  `id_personne` INT(11) NOT NULL ,
  PRIMARY KEY (`rue`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `eecole`.`logininfo`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `eecole`.`logininfo` ;

SHOW WARNINGS;
CREATE  TABLE IF NOT EXISTS `eecole`.`logininfo` (
  `login` VARCHAR(45) NOT NULL ,
  `password` VARCHAR(45) NOT NULL ,
  `id_personne` INT(11) NOT NULL ,
  PRIMARY KEY (`login`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

SHOW WARNINGS;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
