package com.ecole.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

public class AbstractDao {

	private Connection connection;
	String schema = "eecole";
	String user = "root";
	String password = "roidecoeur";
	Connection conn = null;
	String url = "jdbc:mysql://10.3.105.69:3307/" + schema;

//	public AbstractDao() {
//		this("password");
//	}

	public AbstractDao() {
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			this.connection = DriverManager.getConnection(url, user, password);
					} 
                catch (Exception e) {
			e.printStackTrace();
		}
	}
        
        
	protected boolean execute(String sql) {
		try {
			return connection.createStatement().executeUpdate(sql)>=0;
		} catch (SQLException e) {
			throw new RuntimeException(e.getMessage());
		}
	}
	

	protected ResultSet executeQuery(String sql) {
		try {
			return connection.createStatement().executeQuery(sql);
		} catch (SQLException e) {
			throw new RuntimeException(e.getMessage());
		}
	}

}

