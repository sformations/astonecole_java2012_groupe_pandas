package com.ecole.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EcoleContextListener implements ServletContextListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(EcoleContextListener.class);

    
    @Override
    public void contextInitialized(ServletContextEvent sce) {

//                "\n _____             _                    _   \n"
//                + "| ___ \\           | |                  | |  \n"
//                + "| |_/ / __ _ _ __ | | _____  _ __   ___| |_ \n"
//                + "| ___ \\/ _` | '_ \\| |/ / _ \\| '_ \\ / _ \\ __|\n"
//                + "| |_/ / (_| | | | |   < (_) | | | |  __/ |_ \n"
//                + "\\____/ \\__,_|_| |_|_|\\_\\___/|_| |_|\\___|\\__|\n"  
//                );
        
//LOGGER.info(
//" _______         _______   ______   ______    __       _______ .______      ___      .__   __.  _______       ___           _______.\n"
//+ "|   ____|       |   ____| /      | /  __  \  |  |     |   ____||   _  \    /   \     |  \ |  | |       \     /   \         /       |\n"
//+ "|  |__    ______|  |__   |  ,----'|  |  |  | |  |     |  |__   |  |_)  |  /  ^  \    |   \|  | |  .--.  |   /  ^  \       |   (----`\n"
//+ "|   __|  |______|   __|  |  |     |  |  |  | |  |     |   __|  |   ___/  /  /_\  \   |  . `  | |  |  |  |  /  /_\  \       \   \    \n"
//+ "|  |____        |  |____ |  `----.|  `--'  | |  `----.|  |____ |  |     /  _____  \  |  |\   | |  '--'  | /  _____  \  .----)   |   \n"
//+ "|_______|       |_______| \______| \______/  |_______||_______|| _|    /__/     \__\ |__| \__| |_______/ /__/     \__\ |_______/    \n"
//);                                                                                                                                    

        LOGGER.info("Ecole PANDA ... has just started.");
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        LOGGER.info("Ecole PANDA application destroyed !");
    }
}
