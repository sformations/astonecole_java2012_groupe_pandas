/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ecole.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import javax.faces.application.FacesMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Administrateur
 */
public class LoginInfoDao extends AbstractDao{
    private static final Logger LOGGER = LoggerFactory.getLogger(LoginInfoDao.class);
    static final FacesMessage LOGIN_FAILED_FACES_MESSAGE = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Login failed", "You login action failed !");
    
    public LoginInfoDao() {
        super();
        LOGGER.debug("new LoginInfoDao:");
    }
    
    public ResultSet findLogin(String login) throws SQLException{
        String sql;
        sql = "select * from logininfos where login='"+login+"'";
        LOGGER.debug("préparation de la requete findLogin:'" + sql);
                
        ResultSet result=executeQuery(sql);
        LOGGER.debug("retour de execution requete findLogin:'" + sql + "' avec resutat:" + result);
        
        
        //pas bon test!!!!!
        if (result !=null){
            return result; 
        }
        else {
            return null;
        }
        
    }
    public ResultSet findTypePersonne(int id) throws SQLException{
        String sql;
        sql = "select type from personne where id="+id;
        LOGGER.debug("préparation de la requete findTypePersonne:'" + sql);
        
        ResultSet result=executeQuery(sql);
        LOGGER.debug("retour de execution requete findTypePersonne:'" + sql + "' avec resutat:" + result);
        if (result !=null){
            return result;
        }
        else {
            return null;
        }
    }
   
}   

