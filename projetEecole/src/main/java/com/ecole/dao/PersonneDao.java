/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ecole.dao;

import com.ecole.model.Personne;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Administrateur
 */
public class PersonneDao extends AbstractDao {

    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(PersonneDao.class);

    public PersonneDao() {
        super();
    }

    public boolean createPersonne(Personne personneForm) {
        boolean retourExecute1 = false;
        boolean retourExecute2 = false;
        boolean retourExecute3 = false;

        String iDretour = "";
        String sqlp = "insert into personne values(0, \n"
                + "'" + personneForm.getNom() + "',\n"
                + "'" + personneForm.getPrenom() + "',\n"
                + "'" + personneForm.getEmail() + "',\n"
                + "'" + personneForm.getPhoto() + "',\n"
                + "'" + personneForm.getTypePersone() + "',\n"
                + "'" + personneForm.getDateNaissance() + "')";
        LOGGER.info("{} sqlp);" + sqlp);
        retourExecute1 = execute(sqlp);
        LOGGER.info("{} retourExecute1 = execute(sqlp);" + retourExecute1);


        iDretour = "(SELECT max(id) from personne )";
        
        String sqla = "insert into adresse values(\n"
                + "'" + personneForm.getAdresse().getRue() + "',\n"
                + "'" + personneForm.getAdresse().getCodepostal() + "',\n"
                + "'" + personneForm.getAdresse().getVille() + "',\n"
                + iDretour + ")";
        LOGGER.info("{} sqlp);" + sqla);
        retourExecute2 = execute(sqla);
        LOGGER.info("{} retourExecute2 = execute(sqla);" + retourExecute2);

        String sqlL = "insert into logininfos values(\n"
                + "'" + personneForm.getLoginInfoPersonne().getLogin() + "',\n"
                + "'" + personneForm.getLoginInfoPersonne().getPassword() + "',\n"
                + iDretour + ")";
        
        LOGGER.info("{} sqlp);" + sqlL);
        retourExecute3 = execute(sqlL);
        LOGGER.info("{} retourExecute3 = execute(sqlL);" + retourExecute3);

        return (retourExecute1 && retourExecute2 && retourExecute3);
    }

   

//    public static void main(String arg[]) {
//        Personne newPersonne = new Personne();
//        PersonneDao pdao = new PersonneDao();
//        
////        try {
////            DateFormat formatter;
////            java.sql.Date dateFormatter;
////            formatter = new SimpleDateFormat("dd-MM-yyyy");
////
////            dateFormatter = (java.sql.Date) formatter.parse("01-12-2012");
//            newPersonne.setNom("Arnaud");
//            newPersonne.setPrenom("Phil");
//            newPersonne.setEmail("phil_a@free.fr");
//            newPersonne.setTypePersone("ETU");
//            newPersonne.setPhoto(null);
//            newPersonne.setDateNaissance("01-12-2012");
//            newPersonne.setAdresse(new Adresse("Leclerc", "92150", "Chatillon"));
//            newPersonne.setLoginInfo(new LoginInfos("aphil", "aa"));
////        } catch (ParseException ex) {
////            Logger.getLogger(PersonneDao.class.getName()).log(Level.SEVERE, null, "ca pete" + ex);
////        }
////        char[] typePersone;
////        String test="ETU";
////        typePersone=test.toCharArray();
////        System.out.println("Le  test : "+ test.toCharArray());
////        
////        System.out.println("Le type personne test : "+ String.valueOf(typePersone));
////        System.out.println("Le nom personne: "+newPersonne.getNom());
////        System.out.println("Le type personne: "+newPersonne.getTypePersone());
//        
//        pdao.createPersonne(newPersonne);
//    }
}
