package com.ecole.dao;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;
import javax.faces.application.FacesMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class AbstractDao {
    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractDao.class);
    static final FacesMessage LOGIN_FAILED_FACES_MESSAGE = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Login failed", "You login action failed !");
    private Connection connection = null;


    public Connection getConnection() {
        return connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }
   
    
//        String url = "jdbc:mysql://10.3.105.69:3307/" + schema;

    public AbstractDao() {

        try {
//            Properties prop = new Properties();
//            FileInputStream in = null;
//            try {
//                
//                in = new FileInputStream("C:/projet_panda/astonecole_java2012_groupe_pandas/projetEecole/src/main/resources/connexionBase");
//                //in = new FileInputStream("UrlBaseMysql.properties");
//                prop.load(in);
//                LOGGER.info("{} conexion Réussie.");
//            } catch (FileNotFoundException ef) {                
//                LOGGER.info("{} erreur de conexion.", ef.getMessage());
//            } catch (IOException e) {
//            } finally {
//                if (in != null) {
//                    try {
//                        in.close();
//                    } catch (IOException e) {
//                        
//                    }
//                }
//            }
            String schema = "eecole";
            String user = "root";
            String password = "roidecoeur";
            String url = "jdbc:mysql://10.3.105.69:3307/" + schema;
            //String url = "jdbc:mysql://169.254.88.233:3307/" + schema;
    
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            //String url = prop.getProperty("url") + schema;
            this.setConnection(DriverManager.getConnection(url, user, password));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected boolean execute(String sql) {
        try {
            return getConnection().createStatement().executeUpdate(sql) >= 0;
        } catch (SQLException e) {
            LOGGER.info("{} erreur de executeUpdate.", e.getMessage());
            throw new RuntimeException(e.getMessage());
        }
    }

    protected ResultSet executeQuery(String sql) {
        try {
            return getConnection().createStatement().executeQuery(sql);
        } catch (SQLException e) {
            LOGGER.info("{} erreur de executeQuery.", e.getMessage());
            throw new RuntimeException(e.getMessage());
        }
    }
    public void deconnect(){
        try { 
            this.getConnection().close();
            LOGGER.info("{} Deconnexion reussie.");
        } catch (SQLException ex) {
            java.util.logging.Logger.getLogger(LoginInfoDao.class.getName()).log(Level.SEVERE, null, ex);
        }
   } 
}
