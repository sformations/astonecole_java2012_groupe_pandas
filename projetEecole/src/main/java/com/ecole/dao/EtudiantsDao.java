/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ecole.dao;

import com.ecole.model.Etudiant;
import java.sql.ResultSet;
import java.util.Map;
import javax.faces.context.FacesContext;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Administrateur
 */
public class EtudiantsDao extends AbstractDao {

    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(PersonneDao.class);

    public EtudiantsDao() {
        super();
    }

    public ResultSet findEtudiant(String login) {
        
        
        String sql = "select * from personne as p \n"
                + "inner join logininfos  as l on l.id_personne=p.id \n"
                + "left join adresse as a on a.id_personne=p.id \n"
                + "where type='ETU'"
                + "and l.login='" + login + "'";
        ResultSet result = executeQuery(sql);

        if (result != null) {
            return result;
        } else {
            return null;
        }
    }

    public ResultSet findAllEudiants(String type) {

        String sql = "select * from personne as p \n"
                + "inner join logininfos  as l on l.id_personne=p.id \n"
                + "left join adresse as a on a.id_personne=p.id \n"
                + "where type='"+ type +"'";

        ResultSet result = executeQuery(sql);

        if (result != null) {
            return result;
        } else {
            return null;
        }
    }

    public boolean deleteEtudiant(int id) {
        boolean retourExecute1 = false;
        boolean retourExecute2 = false;
        boolean retourExecute3 = false;

        retourExecute1 = execute("delete from adresse where id_personne=" + id);
        LOGGER.info("{} retourExecute1 = execute(delAdr);" + retourExecute1);

        retourExecute2 = execute("delete from logininfos where id_personne=" + id);
        LOGGER.info("{} retourExecute1 = execute(delLoginInfos);" + retourExecute2);

        retourExecute3 = execute("delete from personne where id=" + id);
        LOGGER.info("{} retourExecute1 = execute(delPers);" + retourExecute3);

        return (retourExecute1 && retourExecute2 && retourExecute3);
    }

    public boolean updateEtudiant() {

        String sql = "update personne set nom='";
        return true;
    }

    public boolean createEtudiant(Etudiant etudiant) {
        String sql = "insert into personne values()";
        return (execute(sql));

    }
}
