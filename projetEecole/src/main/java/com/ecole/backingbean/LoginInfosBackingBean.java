/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ecole.backingbean;

import com.ecole.dao.*;
import com.ecole.model.LoginInfos;
import com.ecole.model.Personne;
import com.ecole.model.SHA1Pwd;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Administrateur
 */
public class LoginInfosBackingBean {

    private static final Logger LOGGER = LoggerFactory.getLogger("LIBB");
    static final FacesMessage LOGIN_FAILED_FACES_MESSAGE = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Login failed", "You login action failed !");
    private String loginForm;
    private String passwordForm;    
    private String typePersonneForm;
    private Personne personneForm;
    private String PersonneRetourOKForm;

    public String getPersonneRetourOKForm() {
        return PersonneRetourOKForm;
    }

    public void setPersonneRetourOKForm(String PersonneRetourOKForm) {
        this.PersonneRetourOKForm = PersonneRetourOKForm;
    }

    public Personne getPersonneForm() {
        return personneForm;
    }

    public void setPersonneForm(Personne personneForm) {
        this.personneForm = personneForm;
    }

    public String getTypePersonneForm() {
        return typePersonneForm;
    }

    public void setTypePersonneForm(String typePersonneForm) {
        this.typePersonneForm = typePersonneForm;
    }

    //private LoginInfos dbloginInfos = new LoginInfos() ;
    public String getLoginForm() {
        return loginForm;
    }

    public void setLoginForm(String loginForm) {
        this.loginForm = loginForm;
    }

    public String getPasswordForm() {
        return passwordForm;
    }

    public void setPasswordForm(String passwordForm) {
        LOGGER.debug("New passwordForm:" + passwordForm);
        this.passwordForm = passwordForm;
    }

    /**
     * Creates a new instance of LoginInfosBackingBean
     */
    public LoginInfosBackingBean() {
        LOGGER.debug("New LoginInfosBackingBean instance created !");
    }

    public LoginInfos loginInfosBB() throws SQLException {
        LoginInfoDao lDao = new LoginInfoDao();
        LOGGER.info("{} création du lDao du loginInfosBB:" + lDao);

        LoginInfos dbl = new LoginInfos();
        LOGGER.info("{} création du dbl :" + dbl);

        ResultSet result;
        result = lDao.findLogin(getLoginForm());
        LOGGER.info("{} retour de la récupération du resultset :" + result + " avec le login:" + getLoginForm());

        /* faire un test sur objet vide ou sur le nombre de lignes que contient le resutset */
        if (result != null) {
            //tester si c'est null
            // if(result.first()){
            //  while(result.next()){
            try {
                result.next();
                //if (result.getString("login")!=""){

                dbl.setLogin(result.getString("login"));
                dbl.setPassword(result.getString("password"));
                dbl.setId(result.getInt("id_personne"));
                LOGGER.info("{} renvoie login BD :" + dbl.getLogin() + " | " + dbl.getPassword() + " | " + dbl.getId());
                return dbl;
            } catch (SQLException e) {
                LOGGER.info("{} renvoie login inexistant :");
                //dbl.setError(true);
                return dbl;

            }
        } else {
            LOGGER.info("{} result est null :");
            return null;
        }
    }

    public String checkLogin() throws SQLException {
        String ChaineRetour = "invalide";

        LoginInfos dblg = loginInfosBB();
        LOGGER.info("{} retour du loginInfosBB :" + dblg);
        
        LOGGER.info("Avant Test password password du formulaire:" + getPersonneRetourOKForm());      
        
        if (dblg != null) {
            if (getLoginForm().equalsIgnoreCase(dblg.getLogin())) {
                LOGGER.info("{} Login ok :" + dblg.getLogin());
                if (getPersonneRetourOKForm().equals(dblg.getPassword())) {
                    LOGGER.info("{} password ok :" + dblg.getPassword());
//                    ChaineRetour = "valide";
                    /* récupération du type de personne*/
                    LoginInfoDao lDao = new LoginInfoDao();
                    LOGGER.info("{} création du lDao du checkLogin :" + lDao);

                    ResultSet res;
                    res = lDao.findTypePersonne(dblg.getId());
                    LOGGER.info("{} retour du findTypePersonne : " + res + " par rapport a l'ID:" + dblg.getId());

                    if (res != null) {
                        //if(!res.isBeforeFirst()){
                        res.next();
                        if (res.getString("type").equalsIgnoreCase("ADM")){
                            setTypePersonneForm(res.getString("type"));
                            LOGGER.info("{} TypePersonneForm = " + typePersonneForm);
                        }else{
                            //cas du type ETU ou ENS                            
                                    setTypePersonneForm(res.getString("type"));
                                    LOGGER.info("{} TypePersonneForm = " + typePersonneForm);
                               
                            
                        }
                        ChaineRetour = res.getString("type");
                        LOGGER.info("{} ChaineRetour = " + res.getString("type"));
                        //}
                    }
                    lDao.deconnect();
                } else {
                    LOGGER.info("{} conexion Réussie.");
                    Properties prop = new Properties();
                    FileInputStream cfi = null;
                    try {

                        //in = new FileInputStream("necole_java2012_groupe_pandas/projetEecole/src/main/resources/connexionBase");
                        cfi = new FileInputStream("Messages.properties");
                        LOGGER.info("contenu du fichier cfi:"+cfi);
                        
                        prop.load(cfi);
                        LOGGER.info("contenu de la propriete prop:"+prop.getProperty("password_error"));
                    } catch (FileNotFoundException ef) {
                        LOGGER.info("{} erreur de conexion.", ef.getMessage());
                    } catch (IOException e) {
                        LOGGER.info("{} erreur de conexion.", e.getMessage());
                    }


                    LOGGER.info("{} mauvais mot de passe :" + dblg.getPassword());

                    FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_WARN, prop.getProperty("password_error"), prop.getProperty("password_error"));
                    FacesContext.getCurrentInstance().addMessage("loginForm:passwordInputText", msg);

                    ChaineRetour = "ErrorPwd";
                }
            } else {
                LOGGER.info("{} mauvais Login :" + dblg.getLogin());
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "erreur de login", "le login est incorrect");
                FacesContext.getCurrentInstance().addMessage("loginForm:loginInputText", msg);
            }
        }
        return ChaineRetour;
    }
    public String doDisconnect(){
     ((HttpSession) FacesContext.getCurrentInstance()
         .getExternalContext()
         .getSession(true)).invalidate();
      // Permet de rerouter l'utilisateur sur la page d'accueil
      return "index.xhtml";
   }
//    public static void main(String arg[]) throws SQLException{
////        String login="";
//        LoginInfoDao lDao=new LoginInfoDao();
//        ResultSet result;
//        result = lDao.findLogin("lpierre");
//        LoginInfos dblg = new LoginInfos();
//        dblg=loginInfosBB();        
//        System.out.println("login :"+dblg.getLogin());
//        System.out.println("pwd :"+dblg.getMotDePasse());
//    }
}
