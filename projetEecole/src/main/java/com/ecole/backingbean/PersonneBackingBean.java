/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ecole.backingbean;

import com.ecole.dao.PersonneDao;
import com.ecole.model.Admin;
import com.ecole.model.Adresse;
import com.ecole.model.Enseignant;
import com.ecole.model.Etudiant;
import com.ecole.model.LoginInfos;
import com.ecole.model.Personne;
import java.awt.Image;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.sql.Date;
import java.text.ParseException;
import java.util.logging.Level;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.DataModel;
import javax.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Administrateur
 */
public class PersonneBackingBean {

    private static final Logger LOGGER = LoggerFactory.getLogger(PersonneBackingBean.class);
    private String loginForm;
    private String passwordForm;
    private String passwordFormComfirm;
    private String nomForm;
    private String prenomForm;
    private String datenaissanceForm;

    public String getPasswordFormComfirm() {
        return passwordFormComfirm;
    }

    public void setPasswordFormComfirm(String passwordFormComfirm) {
        this.passwordFormComfirm = passwordFormComfirm;
    }
    private String emailForm;
    private Image photoForm;
    private String typePersonneForm;
    private String rueForm;
    private String cpForm;
    private String villeForm;
    private String PersonneRetourOKForm;

    public String getPersonneRetourOKForm() {
        return PersonneRetourOKForm;
    }

    public void setPersonneRetourOKForm(String PersonneRetourOKForm) {
        this.PersonneRetourOKForm = PersonneRetourOKForm;
    }

    public PersonneBackingBean() {
        LOGGER.info("création PersonneBackingBean init");
    }

    public String getLoginForm() {
        return loginForm;
    }

    public void setLoginForm(String loginForm) {
        LOGGER.info("création loginForm" + loginForm);
        this.loginForm = loginForm;
    }

    public String getPasswordForm() {
        return passwordForm;
    }

    public void setPasswordForm(String passwordForm) {
        LOGGER.info("création passwordForm" + passwordForm);
        this.passwordForm = passwordForm;
    }

    public String getNomForm() {
        return nomForm;
    }

    public void setNomForm(String nomForm) {
        LOGGER.info("création nomForm" + nomForm);
        this.nomForm = nomForm;
    }

    public String getPrenomForm() {
        return prenomForm;
    }

    public void setPrenomForm(String prenomForm) {
        LOGGER.info("création prenomForm" + prenomForm);
        this.prenomForm = prenomForm;
    }

    public String getDatenaissanceForm() {
        return datenaissanceForm;
    }

    public void setDatenaissanceForm(String datenaissanceForm) {
        LOGGER.info("création datenaissanceForm" + datenaissanceForm);
        this.datenaissanceForm = datenaissanceForm;
    }

    public String getEmailForm() {
        return emailForm;
    }

    public void setEmailForm(String emailForm) {
        LOGGER.info("création emailForm" + emailForm);
        this.emailForm = emailForm;
    }

    public Image getPhotoForm() {
        return photoForm;
    }

    public void setPhotoForm(Image photoForm) {
        LOGGER.info("création photoForm" + photoForm);
        this.photoForm = photoForm;
    }

    public String getTypePersonneForm() {
        return typePersonneForm;
    }

    public void setTypePersonneForm(String typePersonneForm) {
        LOGGER.info("création typePersonneForm" + typePersonneForm);
        this.typePersonneForm = typePersonneForm;
    }

    public String getRueForm() {
        return rueForm;
    }

    public void setRueForm(String rueForm) {
        LOGGER.info("création rueForm" + rueForm);
        this.rueForm = rueForm;
    }

    public String getCpForm() {
        return cpForm;
    }

    public void setCpForm(String cpForm) {
        LOGGER.info("création cpForm" + cpForm);
        this.cpForm = cpForm;
    }

    public String getVilleForm() {
        return villeForm;
    }

    public void setVilleForm(String villeForm) {
        LOGGER.info("création villeForm" + villeForm);
        this.villeForm = villeForm;
    }

    public void updateTypePersonneForm(ActionEvent event) {
        setTypePersonneForm(getTypePersonneForm());
        //LOGGER.info("updateSelected " + (Etudiant) dataModel.getRowData() + "-> Etudiant : " + selectedEtudiant.getNom());
    }

    public void reInitForm() {

        setNomForm("");
        setPrenomForm("");
        setEmailForm("");
        setLoginForm("");
        setPasswordForm("");
        setRueForm("");
        setCpForm("");
        setVilleForm("");
        setDatenaissanceForm("");
        setTypePersonneForm("");
        setPersonneRetourOKForm("");
    }

    public String addPersonne() {
        String ChaineRetour = "PersonneCree";
//        try {
        LOGGER.info("{} Debut de la création ");
        PersonneDao pDao = new PersonneDao();
//            DateFormat formatter ; 
//            Date dateFormatter ; 
//             formatter = new SimpleDateFormat("dd/MMM/yy");

//             dateFormatter = (Date)formatter.parse(getDatenaissanceForm());
        Personne newPersonne = new Personne();


        newPersonne.setNom(getNomForm());
        newPersonne.setPrenom(getPrenomForm());
        newPersonne.setPhoto(getPhotoForm());
        newPersonne.setDateNaissance(getDatenaissanceForm());
        newPersonne.setAdresse(new Adresse(getRueForm(), getCpForm(), getVilleForm()));
        newPersonne.setLoginInfoPersonne(new LoginInfos(getLoginForm(), getPersonneRetourOKForm()));
        newPersonne.setEmail(getEmailForm());
        newPersonne.setTypePersone(getTypePersonneForm());
        LOGGER.info("{} création de la newPersonne:" + newPersonne);
        pDao.createPersonne(newPersonne);
//        } catch (ParseException ex) {
//            java.util.logging.Logger.getLogger(PersonneBackingBean.class.getName()).log(Level.SEVERE, null, ex);
//        }
        setPersonneRetourOKForm("OK");
        reInitForm();
        return ChaineRetour;
    }

    public String doDisconnect() {
        ((HttpSession) FacesContext.getCurrentInstance()
                .getExternalContext()
                .getSession(true)).invalidate();
        // Permet de rerouter l'utilisateur sur la page d'accueil
        return "../index.xhtml";
    }
}
