/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ecole.model;


//import com.ecole.backingbean.ListeBackingBean;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SHA1Pwd {
private static final Logger LOGGER = LoggerFactory.getLogger(SHA1Pwd.class);

public static String encrypt(String passwordChar) {

        passwordChar = null;
        //Console console = System.console();
        MessageDigest md = null;
        MessageDigest md2 = null;
        //String encodedSetPwdInHexString = "FbEbFdjdAdhFBcdCBEFBdebDjacgbdfj";

//digest value of the string � password. change it to your required password digest.

        try {

            md = MessageDigest.getInstance("Sha1"); // can be replaced with MD5
            md2 = MessageDigest.getInstance("MD5"); // can be replaced with MD5
                        
// SHA1 has fewer collisions in comparison with MD5.

        } catch (NoSuchAlgorithmException e1) {
            e1.printStackTrace();
        }
        byte[] passwordByte = passwordChar.getBytes();
        md.update(passwordByte, 0, passwordByte.length);    
        md2.update(passwordByte, 0, passwordByte.length);
        byte[] encodedPassword = md2.digest(md.digest());         
        String encodedPasswordInString = toHexString(encodedPassword);
        LOGGER.debug("password:" + encodedPasswordInString);
        return encodedPasswordInString;  
    }

// method converts Byte Array To a HEX-String
    public static String toHexString(byte[] buf) {
        char[] hexChar = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
            '1', '2', '3', '4', '5', '6'};

        StringBuffer strBuf = new StringBuffer(buf.length * 32);
        for (int i = 0; i < buf.length; i++) {
            strBuf.append(hexChar[(buf[i] & 0xf0) >>> 4]); // fill left with
// zero bits
            //strBuf.append(':');
            strBuf.append(hexChar[buf[i] & 0x0f]);
        }
        return strBuf.toString();
    }
}