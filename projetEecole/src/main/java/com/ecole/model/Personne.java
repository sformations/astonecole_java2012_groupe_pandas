/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ecole.model;

import java.awt.Image;
import java.sql.Date;

/**
 *
 * @author Administrateur
 */
public class Personne {
    
  private String nom;
  private String prenom;
  private String email;
  private Image photo;
  private String typePersone;
  //private java.sql.Date dateNaissance;
  private String dateNaissance;

    public String getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(String dateNaissance) {
        this.dateNaissance = dateNaissance;
    }
  private Adresse adressePersonne;
  private LoginInfos loginInfoPersonne;

  public Personne(String nom, String prenom, String email, Image photo, String typePersone,String dateNaissance,Adresse adresse, LoginInfos loginInfo) {
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
        this.photo = photo;
        
        this.typePersone = typePersone;    
        this.dateNaissance = dateNaissance;
        this.adressePersonne = adresse;
        this.loginInfoPersonne = loginInfo;
    }
  		
   public Personne(){
   } 

    public String getTypePersone() {
        return this.typePersone;
    }

    public void setTypePersone(String typePersone) {
        this.typePersone = typePersone;
    }

    public Adresse getAdressePersonne() {
        return adressePersonne;
    }

    public void setAdressePersonne(Adresse adressePersonne) {
        this.adressePersonne = adressePersonne;
    }

    public LoginInfos getLoginInfoPersonne() {
        return loginInfoPersonne;
    }

    public void setLoginInfoPersonne(LoginInfos loginInfoPersonne) {
        this.loginInfoPersonne = loginInfoPersonne;
    }

    public int getLoginInfoId() {
        return loginInfoPersonne.getId();
    }

    public void setLoginInfoId(int loginInfoId) {
        this.loginInfoPersonne.setId(loginInfoId);
    }

    public Adresse getAdresse() {
        return adressePersonne;
    }

    public void setAdresse(Adresse adresse) {
        this.adressePersonne = adresse;
    }
  
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getEmail() {
        return email;
    }

    public Image getPhoto() {
        return photo;
    }

    public void setPhoto(Image photo) {
        this.photo = photo;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
