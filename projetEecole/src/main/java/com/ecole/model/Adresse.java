/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ecole.model;

/**
 *
 * @author Administrateur
 */
public class Adresse {

  private String rue;
  private String codepostal;
  private String ville;

    public String getRue() {
        return rue;
    }

    public void setRue(String rue) {
        this.rue = rue;
    }

    public String getCodepostal() {
        return codepostal;
    }

    public void setCodepostal(String codepostal) {
        this.codepostal = codepostal;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }
    
   public Adresse(){
   }  
      public Adresse(String rue,String codepostal,String ville){
          this.rue = rue;
          this.codepostal = codepostal;
          this.ville = ville;
   } 
}

