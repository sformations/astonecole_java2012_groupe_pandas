/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ecole.model;

import com.ecole.dao.LoginInfoDao;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Administrateur
 */
public class LoginInfos {
    
  private String password;
  private String login;
  private int id;
  private boolean error;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {        
        this.password = password;
    }

    public LoginInfos(int id,String password, String login) {
        this.password = password;
        this.login = login;
        this.id = id;
    }
    public LoginInfos(String password, String login) {
        this.password = password;
        this.login = login;        
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }
    
    public LoginInfos() {
        //loginInfosBB();
   } 
//    private void loginInfosBB(){
//        try{
//        LoginInfoDao lDao=new LoginInfoDao();       
//        
//        ResultSet result;
//        result = lDao.findLogin(getLogin());
//        
//        result.next();
//                
//        this.setLogin( result.getString("login"));
//        this.setPassword( result.getString("password"));       
//        }
//        catch(SQLException e){
//            e.getStackTrace();
//        }
//    }
}
